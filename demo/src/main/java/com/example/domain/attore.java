package com.example.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="attori")
//Lombok
@Getter @Setter @ToString
public class attore {
    Long codAttore;
    String nome;
    Long annoNascita;
    String country; 
}
