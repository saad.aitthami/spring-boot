package com.example.service;

import java.util.List;

import com.example.domain.attore;
import com.example.repository.attorerepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service //logica di business
public class attoreservice {
    @Autowired
    attorerepository attorerepository; //di del repository

    public List<attore> findAll(){
       return attorerepository.findAll();
    }
}
